#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2023-2024 Nirvati and contributors
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -euo pipefail

# Install Nirvati
if [[ $UID != 0 ]]; then
    echo "Nirvati must be installed as root"
    exit 1
fi

STORAGE_LAYER=${STORAGE_LAYER:-"v1"}
NIRVATI_VERSION=${NIRVATI_VERSION:-"stable"}

echo
echo "======================================"
echo "============ INSTALLING =============="
echo "======== NIRVATI ENTERPRISE =========="
echo "======================================"
echo

if ! command -v git >/dev/null 2>&1; then
  echo "Nirvati requires git to be available on your system!"
  echo "On Debian/Ubuntu, you can install git with:"
  echo "sudo apt install git"
  echo "---"
  echo "On Fedora/CentOS/RHEL, you can install git with:"
  echo "sudo dnf install git"
  echo "---"
  echo "On OpenSUSE, you can install git with:"
  echo "sudo zypper install git"
  echo "---"
  exit 1
fi

if ! command -v mount.nfs >/dev/null 2>&1; then
  echo "Nirvati requires nfs to be available on your system!"
  echo "On Debian/Ubuntu, you can install nfs-common with:"
  echo "sudo apt install nfs-common"
  echo "---"
  echo "On Fedora/CentOS/RHEL, you can install nfs-utils with:"
  echo "sudo dnf install nfs-utils"
  echo "---"
  echo "On OpenSUSE, you can install nfs-client with:"
  echo "sudo zypper install nfs-client"
  echo "---"
  exit 1
fi

if ! command -v iscsiadm >/dev/null 2>&1; then
  echo "Nirvati requires open-iscsi to be available on your system!"
  echo "On Debian/Ubuntu, you can install open-iscsi with:"
  echo "sudo apt install open-iscsi"
  echo "---"
  echo "On Fedora/CentOS/RHEL, you can install iscsi-initiator-utils with:"
  echo "sudo dnf install iscsi-initiator-utils"
  echo "---"
  echo "On OpenSUSE, you can install open-iscsi with:"
  echo "sudo zypper install open-iscsi"
  echo "---"
  exit 1
fi


# Check if running on Ubuntu, and if so, check if the user has installed a linux-modules-extra package
if [ -f /etc/os-release ]; then
  . /etc/os-release
  if [[ ${ID_LIKE:-""} == *"ubuntu"* ]]; then
    if ! dpkg -l | grep linux-modules-extra >/dev/null 2>&1; then
      echo "Nirvati requires the linux-modules-extra package to be installed!"
      echo "You can install it with:"
      echo "sudo apt install linux-modules-extra-$(uname -r)"
      echo "---"
      exit 1
    fi
  fi
fi

# Check if kubectl exists or /usr/local/bin/kubectl exists
KUBECTL=kubectl
if ! command -v kubectl >/dev/null 2>&1; then
  if [ -f /usr/local/bin/kubectl ]; then
    KUBECTL=/usr/local/bin/kubectl
  fi
fi

IS_RKE2=false

if [ "${STORAGE_LAYER}" = "spdk" ]; then
  # Required for Longhorn
  echo 1024 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
fi

$KUBECTL version --client >/dev/null 2>&1 || {
  IS_RKE2=true

  mkdir -p /etc/rancher/rke2

  curl -o /etc/rancher/rke2/config.yaml -fsSL https://get.nirvati.org/rke2-config.yml

  echo "Installing RKE2..."
  echo 
  curl -sfL https://get.rke2.io | INSTALL_RKE2_CHANNEL="latest" sh -
  
  if systemctl is-active --quiet firewalld; then
    echo "Nirvati is not compatible with firewalld"
    exit 1
  fi

  systemctl enable --now rke2-server.service

  KUBECTL=/var/lib/rancher/rke2/bin/kubectl
  export KUBECONFIG=/etc/rancher/rke2/rke2.yaml

  echo "Installing Traefik"
  cat <<EOF | $KUBECTL apply -f -
apiVersion: helm.cattle.io/v1
kind: HelmChart
metadata:
  name: traefik
  namespace: kube-system
spec:
  repo: https://traefik.github.io/charts
  chart: traefik
  valuesContent: |-
    globalArguments:
    - "--providers.kubernetescrd.allowCrossNamespace=true"
    - "--providers.kubernetescrd.allowExternalNameServices=true"
    service:
      annotations:
        metallb.universe.tf/allow-shared-ip: "ip_set_main"
EOF

  echo "Installing Cilium CLI"
  CILIUM_CLI_VERSION=$(curl -s https://raw.githubusercontent.com/cilium/cilium-cli/main/stable.txt)
  CLI_ARCH=amd64
  if [ "$(uname -m)" = "aarch64" ]; then CLI_ARCH=arm64; fi
  curl -L --fail --remote-name-all https://github.com/cilium/cilium-cli/releases/download/${CILIUM_CLI_VERSION}/cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}
  sha256sum --check cilium-linux-${CLI_ARCH}.tar.gz.sha256sum
  sudo tar xzvfC cilium-linux-${CLI_ARCH}.tar.gz /usr/local/bin
  rm cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}
}

if [ "$IS_RKE2" = false ]; then
  echo "Nirvati currently does not work on existing Kubernetes clusters."
  exit 1
fi

if ! command -v tailscale >/dev/null 2>&1; then
  echo "Installing Tailscale..."
  curl -fsSL https://tailscale.com/install.sh | sh
fi

if $KUBECTL get ns nirvati >/dev/null 2>&1; then
  echo "Nirvati seems to be already installed!"
  echo "For updating, please instead use the built-in update method of Nirvati."
  exit 1
fi

if $IS_RKE2; then
  echo "Waiting for Traefik to finish installing..."
  echo "This may take a while, so please be patient."
  while ! $KUBECTL get crd ingressroutes.traefik.io >/dev/null 2>&1; do sleep 1; done
fi

IP_ADDR=$(ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}')


cat <<EOF | $KUBECTL apply -f -
apiVersion: v1
kind: Namespace
metadata:
  name: metallb-system
EOF

echo
echo "Configuring Kubernetes deployments..."
echo
if [ "${STORAGE_LAYER}" = "v1" ]; then
  if [ "${NIRVATI_VERSION}" = "dev" ]; then
    $KUBECTL apply -f https://get.nirvati.org/init-longhorn-1-dev.yml
  else
    $KUBECTL apply -f https://get.nirvati.org/init-longhorn-1.yml
  fi
else
  if [ "${NIRVATI_VERSION}" = "dev" ]; then
    $KUBECTL apply -f https://get.nirvati.org/init-dev.yml
  else
    $KUBECTL apply -f https://get.nirvati.org/init.yml
  fi
fi

echo "Waiting for Nirvati to be ready..."
echo "This can take a few minutes, so please be patient."
while [ ! "$($KUBECTL get deployment -n nirvati main -o jsonpath='{.status.readyReplicas}' 2>/dev/null)" ]; do sleep 1; done
while [ ! "$($KUBECTL get deployment -n nirvati api -o jsonpath='{.status.readyReplicas}' 2>/dev/null)" ]; do sleep 1; done
while [ $($KUBECTL get deployment -n nirvati main -o jsonpath='{.status.readyReplicas}') != 1 ]; do sleep 1; done
while [ $($KUBECTL get deployment -n nirvati api -o jsonpath='{.status.readyReplicas}') != 1 ]; do sleep 1; done

echo "Nirvati is ready!"
echo
echo "You can start the setup at http://$(hostname -I | awk '{print $1}')"
