Deno.serve((req) => {
  const isCurl = req.headers.get("user-agent")?.includes("curl");
  const pathname = new URL(req.url).pathname;
  const decoder = new TextDecoder("utf-8");
  if (pathname === "/install.sh" || (isCurl && pathname === "/")) {
    return new Response(decoder.decode(Deno.readFileSync("install.sh")), {
      headers: new Headers({
        "content-type": "application/x-sh",
      }),
    });
  } else if (pathname === "/get-nirvati.png") {
    return new Response(Deno.readFileSync("get-nirvati.png"), {
      headers: new Headers({
        "content-type": "image/png",
      }),
    });
  } else if (
    pathname === "/install-ee.sh" ||
    (isCurl && (pathname === "/ee" || pathname === "/enterprise"))
  ) {
    return new Response(decoder.decode(Deno.readFileSync("install-ee.sh")), {
      headers: new Headers({
        "content-type": "application/x-sh",
      }),
    });
  } else if (
    pathname === "/install-lite.sh" ||
    (isCurl && pathname === "/lite")
  ) {
    return new Response(
      decoder
        .decode(Deno.readFileSync("install.sh"))
        .replaceAll(
          "https://get.nirvati.org/init-longhorn-1.yml",
          "https://get.nirvati.org/init-lite.yml"
        )
        .replaceAll(
          "https://get.nirvati.org/cilium.yml",
          "https://get.nirvati.org/cilium-no-proxy-replacement.yml"
        )
        .replaceAll("--disable-kube-proxy", ""),
      {
        headers: new Headers({
          "content-type": "application/x-sh",
        }),
      }
    );
  } else if (pathname === "/init.yml") {
    return new Response(decoder.decode(Deno.readFileSync("init.yml")), {
      headers: new Headers({
        "content-type": "text/yaml",
      }),
    });
  } else if (pathname === "/init-citadel.yml") {
    return new Response(decoder.decode(Deno.readFileSync("init-citadel.yml")), {
      headers: new Headers({
        "content-type": "text/yaml",
      }),
    });
  } else if (pathname === "/cilium.yml") {
    return new Response(decoder.decode(Deno.readFileSync("cilium.yml")), {
      headers: new Headers({
        "content-type": "text/yaml",
      }),
    });
  } else if (pathname === "/cilium-modern.yml") {
    return new Response(decoder.decode(Deno.readFileSync("cilium-modern.yml")), {
      headers: new Headers({
        "content-type": "text/yaml",
      }),
    });
  } else if (pathname === "/cilium-no-proxy-replacement.yml") {
    return new Response(
      decoder.decode(Deno.readFileSync("cilium-no-proxy-replacement.yml")),
      {
        headers: new Headers({
          "content-type": "text/yaml",
        }),
      }
    );
  } else if (pathname === "/init-dev.yml") {
    return new Response(
      decoder
        .decode(Deno.readFileSync("init.yml"))
        .replace("value: stable", "value: main"),
      {
        headers: new Headers({
          "content-type": "text/yaml",
        }),
      }
    );
  } else if (pathname === "/init-lite.yml") {
    return new Response(
      decoder
        .decode(Deno.readFileSync("init.yml"))
        .replace("value: spdk", "value: disabled"),
      {
        headers: new Headers({
          "content-type": "text/yaml",
        }),
      }
    );
  } else if (pathname === "/init-longhorn-1.yml") {
    return new Response(
      decoder
        .decode(Deno.readFileSync("init.yml"))
        .replace("value: spdk", "value: v1"),
      {
        headers: new Headers({
          "content-type": "text/yaml",
        }),
      }
    );
  } else if (pathname === "/init-longhorn-1-dev.yml") {
    return new Response(
      decoder
        .decode(Deno.readFileSync("init.yml"))
        .replace("value: spdk", "value: v1")
        .replace("value: stable", "value: main"),
      {
        headers: new Headers({
          "content-type": "text/yaml",
        }),
      }
    );
  } else if (pathname === "/nirvati-dns.yml") {
    return new Response(decoder.decode(Deno.readFileSync("nirvati-dns.yml")), {
      headers: new Headers({
        "content-type": "text/yaml",
      }),
    });
  } else if (pathname === "/rke2-config.yml") {
    return new Response(decoder.decode(Deno.readFileSync("rke2-config.yml")), {
      headers: new Headers({
        "content-type": "text/yaml",
      }),
    });
  } else if (pathname === "/registries.yaml") {
    return new Response(decoder.decode(Deno.readFileSync("registries.yaml")), {
      headers: new Headers({
        "content-type": "text/yaml",
      }),
    });
  } else if (pathname === "/index.css") {
    return new Response(Deno.readFileSync("index.css"), {
      headers: new Headers({
        "content-type": "text/css",
      }),
    });
  } else if (pathname === "/enterprise" || pathname === "/ee") {
    return new Response(
      decoder
        .decode(Deno.readFileSync("index.html"))
        .replace(
          "https://get.nirvati.org | sudo bash",
          "https://get.nirvati.org/ee | sudo bash"
        ),
      {
        headers: new Headers({
          "content-type": "text/html",
        }),
      }
    );
  } else if (pathname === "/lite") {
    return new Response(
      decoder
        .decode(Deno.readFileSync("index.html"))
        .replace(
          "https://get.nirvati.org | sudo bash",
          "https://get.nirvati.org/lite | sudo bash"
        ),
      {
        headers: new Headers({
          "content-type": "text/html",
        }),
      }
    );
  } else {
    return new Response(Deno.readFileSync("index.html"), {
      headers: new Headers({
        "content-type": "text/html",
      }),
    });
  }
});
