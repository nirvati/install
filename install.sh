#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2023-2024 Nirvati and contributors
#
# SPDX-License-Identifier: GPL-3.0-or-later

set -euo pipefail

# Install Nirvati
if [[ $UID != 0 ]]; then
    echo "Nirvati must be installed as root"
    exit 1
fi

STORAGE_LAYER=${STORAGE_LAYER:-"v1"}
NIRVATI_VERSION=${NIRVATI_VERSION:-"stable"}

echo
echo "======================================"
echo "============ INSTALLING =============="
echo "============= NIRVATI ================"
echo "======================================"
echo

if ! command -v mount.nfs >/dev/null 2>&1; then
  echo "Nirvati requires nfs to be available on your system!"
  echo "On Debian/Ubuntu, you can install nfs-common with:"
  echo "sudo apt install nfs-common"
  echo "---"
  echo "On Fedora/CentOS/RHEL, you can install nfs-utils with:"
  echo "sudo dnf install nfs-utils"
  echo "---"
  echo "On OpenSUSE, you can install nfs-client with:"
  echo "sudo zypper install nfs-client"
  echo "---"
  exit 1
fi

if ! command -v iscsiadm >/dev/null 2>&1; then
  echo "Nirvati requires open-iscsi to be available on your system!"
  echo "On Debian/Ubuntu, you can install open-iscsi with:"
  echo "sudo apt install open-iscsi"
  echo "---"
  echo "On Fedora/CentOS/RHEL, you can install iscsi-initiator-utils with:"
  echo "sudo dnf install iscsi-initiator-utils"
  echo "---"
  echo "On OpenSUSE, you can install open-iscsi with:"
  echo "sudo zypper install open-iscsi"
  echo "---"
  exit 1
fi


# Check if running on Ubuntu, and if so, check if the user has installed a linux-modules-extra package
if [ -f /etc/os-release ]; then
  . /etc/os-release
  if [[ ${ID_LIKE:-""} == *"ubuntu"* ]]; then
    if ! dpkg -l | grep linux-modules-extra >/dev/null 2>&1; then
      echo "Nirvati requires the linux-modules-extra package to be installed!"
      echo "You can install it with:"
      echo "sudo apt install linux-modules-extra-$(uname -r)"
      echo "---"
      exit 1
    fi
  fi
fi

# Check if kubectl exists or /usr/local/bin/kubectl exists
KUBECTL=kubectl
if ! command -v kubectl >/dev/null 2>&1; then
  if [ -f /usr/local/bin/kubectl ]; then
    KUBECTL=/usr/local/bin/kubectl
  fi
fi

IS_K3S=false

if [ "${STORAGE_LAYER}" = "spdk" ]; then
  # Required for Longhorn
  echo 1024 > /sys/kernel/mm/hugepages/hugepages-2049kB/nr_hugepages
fi

# Check if firewalld is running, and if so, add the configuration for k3s
if systemctl is-active --quiet firewalld; then
  echo "Nirvati is not compatible with firewalld"
  exit 1
fi

$KUBECTL version --client >/dev/null 2>&1 || {
  IS_K3S=true
  echo "Downloading Nirvati..."
  mkdir -p /var/lib/rancher/k3s/server/manifests/
  mkdir -p /var/lib/rancher/k3s/agent/images/
  mkdir -p /etc/rancher/k3s/
  curl https://get.nirvati.org/registries.yaml -o /etc/rancher/k3s/registries.yaml
  curl https://get.nirvati.org/init-longhorn-1.yml -o /var/lib/rancher/k3s/server/manifests/nirvati-init.yml
  curl https://get.nirvati.org/cilium.yml -o /var/lib/rancher/k3s/server/manifests/cilium.yml
  # Check if we're above kernel 6.8
  if [ "$(uname -r | cut -d. -f1)" -ge 6 ] && [ "$(uname -r | cut -d. -f2)" -ge 8 ]; then
    curl https://get.nirvati.org/cilium-modern.yml -o /var/lib/rancher/k3s/server/manifests/cilium.yml
  else
    curl https://get.nirvati.org/cilium.yml -o /var/lib/rancher/k3s/server/manifests/cilium.yml
  fi
  
  curl https://get.nirvati.org/nirvati-dns.yml -o /var/lib/rancher/k3s/server/manifests/nirvati-dns.yml
  #if [ "$(uname -m)" = "x86_64" ]; then
    #curl -L https://github.com/nirvati/releases/releases/download/v0.1.1/nirvati-images-v0.1.1-amd64.tar.xz  -o /tmp/nirvati-images-v0.1.1-amd64.tar.xz
    #xz -d -v /tmp/nirvati-images-v0.1.1-amd64.tar.xz
    #cp /tmp/nirvati-images-v0.1.1-amd64.tar /var/lib/rancher/k3s/agent/images/nirvati-images-v0.1.1-amd64.tar
    #rm /tmp/nirvati-images-v0.1.1-amd64.tar
  #elif [ "$(uname -m)" = "aarch64" ]; then
    #curl -L https://github.com/nirvati/releases/releases/download/v0.1.1/nirvati-images-v0.1.1-arm64.tar.xz  -o /tmp/nirvati-images-v0.1.1-arm64.tar.xz
    #xz -d -v /tmp/nirvati-images-v0.1.1-arm64.tar.xz
    #cp /tmp/nirvati-images-v0.1.1-arm64.tar /var/lib/rancher/k3s/agent/images/nirvati-images-v0.1.1-arm64.tar
    #rm /tmp/nirvati-images-v0.1.1-arm64.tar
  #fi
  echo "Installing k3s..."
  if [ -f /run/systemd/resolve/resolv.conf ]; then
    curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--resolv-conf /run/systemd/resolve/resolv.conf --disable=metrics-server,local-storage,coredns,servicelb --flannel-backend=none --disable-network-policy --disable-kube-proxy --kubelet-arg eviction-hard=memory.available<100Mi,nodefs.available<20Gi" sh -
  else
    curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable=metrics-server,local-storage,coredns,servicelb --flannel-backend=none --disable-network-policy --disable-kube-proxy --kubelet-arg eviction-hard=memory.available<100Mi,nodefs.available<20Gi" sh -
  fi

  # Wait for k3s to be ready
  echo "Waiting for k3s to be ready..."
  while [ ! -f /etc/rancher/k3s/k3s.yaml ]; do sleep 1; done
  KUBECTL=/usr/local/bin/kubectl
  
  export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
}

if [ "$IS_K3S" = false ]; then
  echo "Nirvati currently does not work on existing Kubernetes clusters."
  exit 1
fi

if ! command -v tailscale >/dev/null 2>&1; then
  echo "Installing Tailscale..."
  curl -fsSL https://tailscale.com/install.sh | sh
fi

echo "Waiting for Nirvati to be ready..."
echo "This can take a few minutes, so please be patient."
while [ ! "$($KUBECTL get deployment -n nirvati main -o jsonpath='{.status.readyReplicas}' 2>/dev/null)" ]; do sleep 1; done
while [ ! "$($KUBECTL get deployment -n nirvati api -o jsonpath='{.status.readyReplicas}' 2>/dev/null)" ]; do sleep 1; done
while [ $($KUBECTL get deployment -n nirvati main -o jsonpath='{.status.readyReplicas}') != 1 ]; do sleep 1; done
while [ $($KUBECTL get deployment -n nirvati api -o jsonpath='{.status.readyReplicas}') != 1 ]; do sleep 1; done

echo "Nirvati is ready!"
echo
echo "You can start the setup at http://$(hostname -I | awk '{print $1}')"
